INSTDEPENDS += debhelper
INSTDEPENDS += gem2deb
INSTDEPENDS += rake
INSTDEPENDS += ruby-arel
INSTDEPENDS += ruby-bcrypt
INSTDEPENDS += ruby-builder
INSTDEPENDS += ruby-bundler
INSTDEPENDS += ruby-dalli
INSTDEPENDS += ruby-delayed-job
INSTDEPENDS += ruby-erubis
INSTDEPENDS += ruby-globalid
INSTDEPENDS += ruby-i18n
INSTDEPENDS += ruby-json
INSTDEPENDS += ruby-mail
INSTDEPENDS += ruby-minitest
INSTDEPENDS += ruby-mocha
INSTDEPENDS += ruby-mysql2
INSTDEPENDS += ruby-rack
INSTDEPENDS += ruby-rack-cache
INSTDEPENDS += ruby-rack-test
INSTDEPENDS += ruby-rails-dom-testing
INSTDEPENDS += ruby-rails-html-sanitizer
INSTDEPENDS += ruby-sprockets-rails
INSTDEPENDS += ruby-sqlite3
INSTDEPENDS += ruby-thor
INSTDEPENDS += ruby-thread-safe
INSTDEPENDS += ruby-treetop
INSTDEPENDS += ruby-tzinfo
INSTDEPENDS += sqlite3
