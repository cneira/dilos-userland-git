INSTDEPENDS += debhelper
INSTDEPENDS += dh-exec
INSTDEPENDS += asciidoc
INSTDEPENDS += autoconf
INSTDEPENDS += automake
INSTDEPENDS += bison
INSTDEPENDS += chrpath
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-python
# dh-systemd
INSTDEPENDS += docbook-xsl
INSTDEPENDS += docbook-xml
INSTDEPENDS += flex
INSTDEPENDS += help2man
# inetutils-ping | iputils-ping
# libaio-dev [linux-any]
INSTDEPENDS += libaio-dev
INSTDEPENDS += libbz2-dev
INSTDEPENDS += libcurl4-openssl-dev
# | libcurl3-openssl-dev
INSTDEPENDS += libdbus-glib-1-dev
INSTDEPENDS += libdbus-1-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libltdl3-dev
INSTDEPENDS += libnet1-dev
INSTDEPENDS += libncurses5-dev
# libopenhpi-dev [!hurd-any],
# libopenipmi-dev [!hurd-any],
# libpam0g-dev
INSTDEPENDS += libpam-dev
INSTDEPENDS += libsensors4-dev
# [!hurd-any] | libsensors-dev [!hurd-any],
INSTDEPENDS += libtool
INSTDEPENDS += libsnmp-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libxml2-utils
INSTDEPENDS += lynx
# net-tools
# openssh-client
# perl
# psmisc
INSTDEPENDS += python-dev
INSTDEPENDS += python
INSTDEPENDS += swig
# systemd [linux-any],
INSTDEPENDS += libuuid-dev
INSTDEPENDS += xsltproc
INSTDEPENDS += zlib1g-dev
