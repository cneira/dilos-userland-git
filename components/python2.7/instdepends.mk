INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += quilt
INSTDEPENDS += autoconf
INSTDEPENDS += autotools-dev
INSTDEPENDS += lsb-release
INSTDEPENDS += sharutils
INSTDEPENDS += libreadline-dev
INSTDEPENDS += libtinfo-dev
INSTDEPENDS += libncursesw5-dev
INSTDEPENDS += tk-dev
INSTDEPENDS += blt-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libbz2-dev
INSTDEPENDS += libexpat1-dev
# libbluetooth-dev [linux-any] <!profile.nobluetooth>,
# locales [!armel !avr32 !hppa !ia64 !mipsel],
INSTDEPENDS += libsqlite3-dev
INSTDEPENDS += libffi-dev
# libgpm2 [linux-any],
# mime-support, netbase, net-tools, bzip2, time,
INSTDEPENDS += libdb-dev
INSTDEPENDS += libgdbm-dev
INSTDEPENDS += python
INSTDEPENDS += help2man
# xvfb, xauth
#Build-Depends-Indep:
INSTDEPENDS += python3-sphinx
#
INSTDEPENDS += developer-dtrace
INSTDEPENDS += libtsol-dev
INSTDEPENDS += libxnet-dev
INSTDEPENDS += libsec-dev
INSTDEPENDS += libdlpi-dev
