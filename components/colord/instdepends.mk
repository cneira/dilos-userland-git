INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += intltool
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += liblcms2-dev
INSTDEPENDS += libsqlite3-dev
INSTDEPENDS += docbook-utils
#INSTDEPENDS += libpolkit-gobject-1-dev
INSTDEPENDS += libdbus-1-dev
#INSTDEPENDS += valac
INSTDEPENDS += gobject-introspection
INSTDEPENDS += libgirepository1.0-dev
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += autopoint
# bash-completion
