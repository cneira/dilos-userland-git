INSTDEPENDS += debhelper
INSTDEPENDS += automake
INSTDEPENDS += autotools-dev
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-exec
INSTDEPENDS += doxygen
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libexpat-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += python
# python-dbus
# python-gobject
INSTDEPENDS += xmlto
INSTDEPENDS += xsltproc

INSTDEPENDS += libuuid-dev
