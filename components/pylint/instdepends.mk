INSTDEPENDS += debhelper
INSTDEPENDS += python-all
INSTDEPENDS += python3-all
INSTDEPENDS += dh-python
#Build-Depends-Indep:
INSTDEPENDS += python-astroid
INSTDEPENDS += python3-astroid
# python-unittest2
INSTDEPENDS += python-six
# python-sphinx
