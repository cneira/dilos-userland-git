INSTDEPENDS += debhelper
# dbus
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += gobject-introspection
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += intltool
INSTDEPENDS += libexpat1-dev
INSTDEPENDS += libgirepository1.0-dev
INSTDEPENDS += libglib2.0-dev
# libglib2.0-doc
# libgtk-3-doc
# libpam0g-dev
INSTDEPENDS += libpam-dev
# libselinux1-dev [linux-any],
# libsystemd-dev [linux-any],
INSTDEPENDS += pkg-config
INSTDEPENDS += xsltproc
