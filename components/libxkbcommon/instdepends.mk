INSTDEPENDS += debhelper
INSTDEPENDS += quilt
INSTDEPENDS += pkg-config
INSTDEPENDS += xutils-dev
INSTDEPENDS += bison
INSTDEPENDS += flex
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxcb-xkb-dev
INSTDEPENDS += x11-xkb-utils
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += x11proto-kb-dev
INSTDEPENDS += xkb-data
# xvfb
INSTDEPENDS += doxygen
# graphviz
