INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libgtk2.0-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += python
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libatk1.0-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += autotools-dev
INSTDEPENDS += dpkg-dev
# libglib2.0-doc,
# libgtk2.0-doc
