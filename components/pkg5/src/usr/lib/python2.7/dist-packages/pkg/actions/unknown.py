#!/usr/bin/python
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

#
# Copyright (c) 2008, 2012, Oracle and/or its affiliates. All rights reserved.
#

"""module describing a unknown packaging object

This module contains the UnknownAction class, which represents a unknown type
of packaging object.  This is used when the client side of the package
publishing transaction is not given enough information to determine what type of
object it is.  No datastreams or attributes aside from a path are stored."""

import generic

class UnknownAction(generic.Action):
        """Class representing a unknown type of packaging object."""

        __slots__ = []

        name = "unknown"
        ordinality = generic._orderdict[name]
