INSTDEPENDS += debhelper
INSTDEPENDS += dh-exec
INSTDEPENDS += dpkg-dev
INSTDEPENDS += patch
INSTDEPENDS += autoconf
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += autotools-dev
INSTDEPENDS += pkg-config
#INSTDEPENDS += ghostscript
INSTDEPENDS += libavahi-client-dev
INSTDEPENDS += libavahi-common-dev
#INSTDEPENDS += libavahi-compat-libdnssd-dev
INSTDEPENDS += libdbus-1-dev
INSTDEPENDS += libfontconfig1-dev
INSTDEPENDS += libfreetype6-dev
INSTDEPENDS += libgnutls28-dev
INSTDEPENDS += libijs-dev
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libldap2-dev
INSTDEPENDS += libkrb5-dev
# libpam0g-dev
INSTDEPENDS += libpam-dev
INSTDEPENDS += libpaper-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libtiff-dev
#INSTDEPENDS += libusb-1.0-0-dev
INSTDEPENDS += po4a
INSTDEPENDS += po-debconf
INSTDEPENDS += poppler-utils
INSTDEPENDS += sharutils
INSTDEPENDS += zlib1g-dev
# Needed for running the tests, not for the build
# cups-filters
# libcupsfilters-dev
