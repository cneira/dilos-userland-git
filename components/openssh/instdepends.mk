INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-exec
# dh-systemd (>= 1.4),
INSTDEPENDS += dpkg-dev
# libaudit-dev [linux-any],
INSTDEPENDS += libedit-dev
INSTDEPENDS += libgtk-3-dev
INSTDEPENDS += libkrb5-dev
# | heimdal-dev
# libpam0g-dev |
INSTDEPENDS += libpam-dev
# libselinux1-dev [linux-any],
INSTDEPENDS += libssl1.0-dev
# | libssl-dev (<< 1.1.0~),
# libsystemd-dev [linux-any],
INSTDEPENDS += libwrap0-dev
# | libwrap-dev,
INSTDEPENDS += zlib1g-dev
#
INSTDEPENDS += libbsm-dev
INSTDEPENDS += libcontract-dev
INSTDEPENDS += dtrace
#INSTDEPENDS += tuntap-dev
