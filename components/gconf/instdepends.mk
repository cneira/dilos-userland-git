INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libdbus-1-dev
INSTDEPENDS += libdbus-glib-1-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libgtk-3-dev
INSTDEPENDS += libpolkit-gobject-1-dev
INSTDEPENDS += libldap2-dev
INSTDEPENDS += intltool
INSTDEPENDS += libxml-parser-perl
INSTDEPENDS += gettext
INSTDEPENDS += python
# libglib2.0-doc
INSTDEPENDS += gobject-introspection
INSTDEPENDS += libgirepository1.0-dev
#
INSTDEPENDS += devscripts
