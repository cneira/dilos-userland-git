INSTDEPENDS += debhelper
# g++ (>= 4:5.2),
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += gettext
INSTDEPENDS += libcppunit-dev
INSTDEPENDS += libncursesw5-dev
INSTDEPENDS += libsigc++-2.0-dev
#Build-Depends-Indep:
INSTDEPENDS += doxygen
#INSTDEPENDS += ikiwiki
#INSTDEPENDS += libhtml-scrubber-perl
INSTDEPENDS += libimage-magick-perl
