INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
# git
# gnupg2,
INSTDEPENDS += openstack-pkg-tools
INSTDEPENDS += python-all
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-setuptools
# python-sphinx
INSTDEPENDS += python3-all
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3-setuptools
# python3-sphinx
#Build-Depends-Indep: python-coverage,
#                     python-fixtures (>= 1.3.1),
#                     python-markupsafe,
#                     python-mock (>= 1.3),
#                     python-pip,
#                     python-six (>= 1.9.0),
#                     python-testresources,
#                     python-testscenarios,
#                     python-testtools (>= 1.4.0),
#                     python-virtualenv,
#                     python-wheel,
#                     python3-fixtures (>= 0.3.14),
#                     python3-markupsafe,
#                     python3-mock (>= 1.3),
#                     python3-pip,
#                     python3-six (>= 1.9.0),
#                     python3-testresources,
#                     python3-testscenarios,
#                     python3-testtools (>= 1.4.0),
#                     python3-virtualenv,
#                     python3-wheel,
#                     subunit (>= 1.1.0),
#                     testrepository,
