INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += pkg-config
INSTDEPENDS += libx11-dev
INSTDEPENDS += x11proto-dri2-dev
INSTDEPENDS += libxext-dev
#Build-Depends-Indep:
INSTDEPENDS += doxygen-latex
INSTDEPENDS += graphviz
INSTDEPENDS += ghostscript
