INSTDEPENDS += debhelper
INSTDEPENDS += cmake
# default-jdk,
INSTDEPENDS += dh-apache2
INSTDEPENDS += help2man
# javahelper (>= 0.37~),
INSTDEPENDS += libcurl4-gnutls-dev
# | libcurl-ssl-dev,
INSTDEPENDS += libfcgi-dev
INSTDEPENDS += liblcms2-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libtiff-dev
# libxerces2-java
INSTDEPENDS += zlib1g-dev
