INSTDEPENDS += debhelper
INSTDEPENDS += python-all
INSTDEPENDS += python3-all
INSTDEPENDS += dh-python
INSTDEPENDS += libssl-dev
# openssl
# python-sphinx
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python-cryptography
INSTDEPENDS += python3-cryptography
INSTDEPENDS += python-six
INSTDEPENDS += python3-six
INSTDEPENDS += python-pytest
INSTDEPENDS += python3-pytest
INSTDEPENDS += python-cffi
INSTDEPENDS += python3-cffi
INSTDEPENDS += python-all-dev
INSTDEPENDS += python3-all-dev
# python-sphinx-rtd-theme
