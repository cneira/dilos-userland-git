INSTDEPENDS += debhelper
INSTDEPENDS += bison
INSTDEPENDS += dh-exec
INSTDEPENDS += dpkg-dev
INSTDEPENDS += ghostscript
#netpbm
#psutils
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxmu-dev
INSTDEPENDS += libxt-dev
INSTDEPENDS += libxaw7-dev
INSTDEPENDS += texinfo
INSTDEPENDS += dh-autoreconf
