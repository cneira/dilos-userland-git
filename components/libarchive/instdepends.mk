INSTDEPENDS += debhelper
INSTDEPENDS += pkg-config
INSTDEPENDS += libbz2-dev
INSTDEPENDS += liblz4-dev
INSTDEPENDS += liblzma-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += zlib1g-dev
# libacl1-dev [!hurd-any]
# e2fslibs-dev
# libattr1-dev
INSTDEPENDS += sharutils
INSTDEPENDS += nettle-dev
INSTDEPENDS += liblzo2-dev
# locales | locales-all,
INSTDEPENDS += dh-autoreconf
