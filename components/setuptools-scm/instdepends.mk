INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += python
INSTDEPENDS += python3-all
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python-py
INSTDEPENDS += python3-py
INSTDEPENDS += python-pytest
INSTDEPENDS += python3-pytest
# git
INSTDEPENDS += mercurial
INSTDEPENDS += subversion
