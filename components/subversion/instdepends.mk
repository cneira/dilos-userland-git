INSTDEPENDS += debhelper
INSTDEPENDS += apache2-dev
INSTDEPENDS += autoconf
INSTDEPENDS += autotools-dev
# bash-completion
INSTDEPENDS += dh-apache2
INSTDEPENDS += dh-python
INSTDEPENDS += doxygen
INSTDEPENDS += libapr1-dev
INSTDEPENDS += libaprutil1-dev
INSTDEPENDS += libdb5.3-dev
INSTDEPENDS += libdbus-1-dev
INSTDEPENDS += libperl-dev
# kdelibs5-dev
# libgnome-keyring-dev
INSTDEPENDS += libsasl2-dev
#INSTDEPENDS += libsasl-dev
INSTDEPENDS += libserf-dev
INSTDEPENDS += libsqlite3-dev
INSTDEPENDS += libtool
# perl
# Minimum version required to run tests
INSTDEPENDS += python-all-dev
INSTDEPENDS += quilt
INSTDEPENDS += rename
INSTDEPENDS += ruby
INSTDEPENDS += ruby-dev
INSTDEPENDS += swig
INSTDEPENDS += zlib1g-dev
