INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
#Build-Depends-Indep:
INSTDEPENDS += maven-repo-helper
INSTDEPENDS += ant
# default-jdk
INSTDEPENDS += libcommons-httpclient-java
INSTDEPENDS += libcommons-lang-java
INSTDEPENDS += libcommons-cli-java
INSTDEPENDS += liboro-java
INSTDEPENDS += libcommons-collections3-java
INSTDEPENDS += libcommons-vfs-java
INSTDEPENDS += junit
INSTDEPENDS += libjsch-java
INSTDEPENDS += libbcprov-java
INSTDEPENDS += libbcpg-java
# default-jdk-doc
INSTDEPENDS += libjsch-agent-proxy-java
