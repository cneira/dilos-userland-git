INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libsnmp-dev
#INSTDEPENDS += libpci-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libjansson-dev
INSTDEPENDS += libevent-dev
INSTDEPENDS += libreadline-dev
#INSTDEPENDS += libbsd-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += check
#
#INSTDEPENDS += driver-network-bpf-dev
