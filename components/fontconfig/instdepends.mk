INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libfreetype6-dev
INSTDEPENDS += libexpat1-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += gperf
INSTDEPENDS += po-debconf
