INSTDEPENDS += debhelper
INSTDEPENDS += dh-exec
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libtiff5-dev
INSTDEPENDS += gobject-introspection
INSTDEPENDS += libgirepository1.0-dev
INSTDEPENDS += xsltproc
# shared-mime-info
INSTDEPENDS += gtk-doc-tools
# libglib2.0-doc
