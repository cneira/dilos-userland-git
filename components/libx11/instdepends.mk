INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += xtrans-dev
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += x11proto-kb-dev
INSTDEPENDS += x11proto-input-dev
INSTDEPENDS += x11proto-xext-dev
INSTDEPENDS += x11proto-xf86bigfont-dev
INSTDEPENDS += libxcb1-dev
INSTDEPENDS += quilt
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += xutils-dev
# specs
INSTDEPENDS += xmlto
INSTDEPENDS += xorg-sgml-doctools
INSTDEPENDS += w3m
