#!/bin/bash
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License, Version 1.0 only
# (the "License").  You may not use this file except in compliance
# with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# For modifying parameters passed to ttymon, do not edit
# this script. Instead use svccfg(1m) to modify the SMF
# repository. For example:
#
# # svccfg
# svc:> select system/console-login
# svc:/system/console-login> setprop ttymon/terminal_type = "xterm"
# svc:/system/console-login> exit

function get_param
{
    local PARAM="$1"
    VAL=$(cat ${BOOTPARAMS} | grep "${PARAM}=" | cut -f2 -d'=')
    echo "$VAL"
}

function do_cmd
{
    local CMD="$*"
    echo "${CMD}" >> /tmp/cmd.log.txt
    eval "$CMD" || break
}

#PATH=/bin:/usr/sbin:/usr/bin:/usr/local/bin:$PATH
#PATH=/usr/bin:/usr/gnu/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin
PATH=/usr/gnu/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin
EDITOR=vi
LOGNAME=root
HOME=/root
PS1="\W> "
TERMINFO=/usr/share/terminfo
#TERMINFO=/usr/gnu/share/terminfo
TERM=sun-color
#export TERM=xterm-color
#export LANG=en_US.UTF-8
#export LC_ALL=en_US.UTF-8
export LC_ALL=C
SHELL=/bin/bash
FIRSTSTART=/.dilos-first-start
NETCONFIG=/.netconfig
AUTOINSTALLER=/.autoinstaller
TRYREBOOT=/.dilos-try-reboot
BOOTPARAMS=/tmp/bootparams.$$

export PATH EDITOR LOGNAME HOME PS1 TERM TERMINFO SHELL

if test ! -f $FIRSTSTART; then
	/sbin/bootparams > $BOOTPARAMS
	unset SUN_PERSONALITY
	mkdir -p /tmp/var_lib
	mkdir -p /tmp/apt
	if [ -f /var_lib.tar.bz2 ]; then
		gtar xf /var_lib.tar.bz2 -C /tmp/var_lib/
		rm -f /var_lib.tar.bz2
		mkdir -p /var/lib/dpkg
		touch /var/lib/dpkg/status
	else
		mkdir -p /var/lib/dpkg
		touch /var/lib/dpkg/status
	fi
	test -f /var/lib/repo/distr-install.sh && cp -f /var/lib/repo/distr-install.sh /tmp/distr-install.sh

	PW=$(get_param root_shadow)
	if [ -n "${PW}" ]; then
		gsed -i "s;^root:;root:${PW};g" /etc/shadow
		chmod 0400 /etc/shadow
		chown root:sys /etc/shadow
	fi

	ROOTSSHKEY=$(get_param root_sshkey)
	if [ -n "${ROOTSSHKEY}" ]; then
		mkdir -p /root/.ssh
		echo "${ROOTSSHKEY}" > /root/.ssh/authorized_keys
		chmod 0600 /root/.ssh/authorized_keys
		chmod 0700 /root/.ssh
	fi

	if test ! -f $NETCONFIG ; then
		DHCP=0
		NETDRV=$(get_param netdrv)
		if [ -n "${NETDRV}" ]; then
			IP=$(get_param ip)
			if [ "x$IP" = "xdhcp" ]; then
				DHCP=1
			fi
			NETMASK=$(get_param netmask)
			GW=$(get_param gw)
			DNS1=$(get_param dns1)
			echo "DNS1=${DNS1}" >> ${BOOTPARAMS}.2
			DNS2=$(get_param dns2)
			echo "DNS2=${DNS2}" >> ${BOOTPARAMS}.2

			do_cmd "ifconfig $NETDRV plumb"
			if (( $DHCP == 1 )); then
				do_cmd "ifconfig $NETDRV dhcp"
			else
				do_cmd "ifconfig $NETDRV $IP netmask $NETMASK up"
				do_cmd "route add default $GW"
				# do_cmd "rm -f /etc/resolv.conf"
				[ -n "${DNS1}" ] && do_cmd "echo \"nameserver ${DNS1}\" >> /etc/resolv.conf"
				[ -n "${DNS2}" ] && do_cmd "echo \"nameserver ${DNS2}\" >> /etc/resolv.conf"
			fi
			cp -f /etc/nsswitch.dns /etc/nsswitch.conf
		fi
		touch $NETCONFIG
	fi

#	/usr/bin/screen -q -T xterm-color </dev/console >/dev/console 2>&1
	if [ -f $AUTOINSTALLER ]; then
		/usr/bin/screen -q </dev/console >/dev/console 2>&1
		/usr/bin/clear >/dev/console
	fi

	sleep 1

	if test -f $TRYREBOOT; then
		touch $FIRSTSTART
		cp /usr/sbin/uadmin /uadmin
		cp /usr/bin/sleep /sleep
#		cp /usr/bin/eject /eject
#		cp /usr/sun/bin/true /true
		cp /usr/bin/true /true
		sync
		reboot -qn 2>/dev/null &
		/sleep 1
#		/eject -f cdrom 2>/dev/null
		echo "Rebooting into new root filesystem now..." >/dev/msglog
		while /true; do /sleep 1; done
		/uadmin 2 8 2>/dev/null
	fi

	echo >/dev/msglog
	echo "Next options are available:" >/dev/msglog
	echo >/dev/msglog
	echo "  * you may safely reboot now by pressing CTRL-ALT-DEL or" >/dev/msglog
	echo "    login as 'root'(password:'123') and type 'reboot' command;" >/dev/msglog
	echo >/dev/msglog
	echo "  * you could login now as 'root'(password:'123') and make" >/dev/msglog
	echo "    additional modifications;" >/dev/msglog
	echo >/dev/msglog
	echo "  * you could execute 'screen'" >/dev/msglog
	echo "    at anytime to repeat installation procedure;" >/dev/msglog
	echo >/dev/msglog
	echo "              Enjoy! From the DilOS Team." >/dev/msglog
	touch $FIRSTSTART
fi

FMRI=svc:/system/console-login

getproparg() {
	val=`svcprop -p $2 $FMRI`
	[ -n "$val" ] && echo $1 $val
}

args="-g"

val=`svcprop -p ttymon/device $FMRI`
# if this isn't set, recover a little
[ -z "$val" ] && val=/dev/console
args="$args -d $val"

args="$args `getproparg -l ttymon/label`"
#args="$args `getproparg -T ttymon/terminal_type`"
args="$args -T $TERM"
args="$args `getproparg -m ttymon/modules`"

val=`svcprop -p ttymon/nohangup $FMRI`
[ "$val" = "true" ] && args="$args -h"

val=`svcprop -p ttymon/timeout $FMRI`
[ -n "$val" -a "$val" != "0" ] && args="$args -t $val"

val=`svcprop -p ttymon/prompt $FMRI`
if [ -n "$val" ]; then
	prompt=`eval echo $val`
	exec /usr/lib/saf/ttymon $args -p "`eval echo $prompt` "
else
	exec /usr/lib/saf/ttymon $args
fi

#export TERM=sun-color
