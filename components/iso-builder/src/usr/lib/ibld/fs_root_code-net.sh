#!/bin/bash
#
# Copyright 2016 DilOS.
# Use is subject to license terms.
#

/sbin/mount -o remount,rw /
/sbin/mount -a
#echo >> /etc/vfstab
/usr/sbin/devfsadm -c disk
/usr/sbin/devfsadm -C
