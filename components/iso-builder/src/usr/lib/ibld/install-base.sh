#!/bin/bash
#
# Copyright (c) 2013 Igor Kozhukhov <ikozhukhov@gmail.com>.  All rights reserved.
# Use is subject to license terms.
#
PATH=/usr/gnu/bin:/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin
export PATH

test "x$1" = x && exit 1
test "x$2" = x && exit 1
test "x$3" = x && exit 1
test "x$4" = x && exit 1

if test "x$3" = "x1"; then
	mkdir -p /tmp/apt-archive.$$
	mkdir -p $1/var/cache/apt/archives/partial
	mount -F lofs /tmp/apt-archive.$$ $1/var/cache/apt/archives
fi
mkdir -p $1$2
mount -F lofs $2 $1$2

MACH=`uname -p`
if [ "$MACH" = "i386" ]; then
    MACH64="amd64"
elif [ "$MACH" = "sparc" ]; then
    MACH64="sparcv9"
fi
PLATFORM=`uname -m`

# required.lst and base.lst are symbolic links to the
# selected user profile
export APTINST=$(cat $2/aptinst.lst)
LOG="$4"
STAMP="${LOG}.start"

. $2/defaults

echo "Starting ..." >> $LOG
date >> $LOG
cat /etc/issue >> $LOG

TARGET="$1"

PKGSPATH="$2"

APT_GET="apt-get -R $TARGET"

echo "$APT_GET update" | tee -a $LOG
$APT_GET update 2>&1 | tee -a $LOG

#echo "Downloading all packages to storage ..." | tee -a $LOG
#CWD=`pwd`
#mkdir -p $TARGET/var/cache/apt/archives
#cd $TARGET/var/cache/apt/archives
#PKGS=`$APT_GET install -s -d -y --force-yes $APTINST | grep Inst | nawk '{print $2}'`
#$APT_GET download -y --force-yes $PKGS
#cd $CWD
sync; sync

echo "$APT_GET install -y --force-yes sunwcsd"  | tee -a $LOG
$APT_GET install -y --force-yes sunwcsd 2>&1 | tee -a $LOG
sync; sync

echo "$APT_GET install -y --force-yes sunwcs bash dpkg system-kernel"  | tee -a $LOG
$APT_GET install -y --force-yes sunwcs bash dpkg system-kernel 2>&1 | tee -a $LOG
sync; sync

echo "$APT_GET install -y --force-yes $APTINST" | tee -a $LOG
$APT_GET install -y --force-yes $APTINST 2>&1 | tee -a $LOG 

echo "$APT_GET clean" | $TEE -a $LOG
$APT_GET clean 2>&1 | $TEE -a $LOG 
sync; sync

for pid in `ps -ef|nawk '/devfsadmd/ {print $2}'`; do
	if pfiles $pid 2>/dev/null | grep $1 >/dev/null; then
		kill -9 $pid 2>/dev/null
		break
	fi
done

sleep 2
umount $1/devices 2>/dev/null
umount -f $1$2 2>/dev/null
if test "x$3" = "x1"; then
	umount -f $1/var/cache/apt/archives
	rm -rf /tmp/apt-archive.$$
fi

cp $LOG $1/root
cp $PKGSLOG $1/root

# temporary fixes
#mkdir -p $1/usr/perl5/bin
#cd $1/usr/perl5/bin; ln -sf /usr/bin/perl perl

# ntp client config
echo "Configuring NTP service ..." | tee -a $LOG
NTPCONFCONF="$1/etc/inet/ntp.conf"
echo "driftfile /etc/inet/ntp.drift" > $NTPCONFCONF
echo "server pool.ntp.org # default" >> $NTPCONFCONF
echo "server 0.pool.ntp.org" >> $NTPCONFCONF
echo "server 1.pool.ntp.org" >> $NTPCONFCONF
echo "server 2.pool.ntp.org" >> $NTPCONFCONF

# update locale
echo "Setup en_US.UTF-8 locale ..." | tee -a $LOG
echo "LANG=en_US.UTF-8" >> "/etc/default/init"
echo "LC_ALL=en_US.UTF-8" >> "/etc/default/init"

echo "Removing stamp file: $STAMP ..." | tee -a $LOG
test -f $STAMP && rm -f $STAMP
sync; sync

exit 0
