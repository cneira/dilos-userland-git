#!/bin/bash
#
# Copyright 2017 Igor Kozhukhov <igor@dilos.org>
# Use is subject to license terms.
#
PATH=/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin
export PATH

test "x$1" = x && exit 1
test "x$2" = x && exit 1
test "x$3" = x && exit 1
test "x$4" = x && exit 1

SLEEP="/usr/bin/sleep"
TEE="/usr/bin/tee"
CAT="/usr/bin/cat"

MACH=`uname -p`
if [ "$MACH" = "i386" ]; then
    MACH64="amd64"
elif [ "$MACH" = "sparc" ]; then
    MACH64="sparcv9"
fi
PLATFORM=`uname -m`

# required.lst and base.lst are symbolic links to the
# selected user profile
export APTINST=$($CAT $2/aptinst.lst)
LOG="$4"
STAMP="${LOG}.start"

. $2/defaults

echo "Starting ..." >> $LOG
date >> $LOG
$CAT /etc/issue >> $LOG

TARGET="$1"

PKGSPATH="$2"

APT_GET="apt-get -R $TARGET"

echo "$APT_GET update" | $TEE -a $LOG
$APT_GET update 2>&1 | $TEE -a $LOG

#echo "Downloading all packages to storage ..." | $TEE -a $LOG
#CWD=`pwd`
#mkdir -p $TARGET/var/cache/apt/archives
#cd $TARGET/var/cache/apt/archives
#PKGS=`$APT_GET install -s -d -y --force-yes $APTINST | grep Inst | nawk '{print $2}'`
#$APT_GET download -y --force-yes $PKGS
#cd $CWD
sync; sync

echo "$APT_GET install -y --force-yes sunwcsd"  | $TEE -a $LOG
$APT_GET install -y --force-yes sunwcsd 2>&1 | $TEE -a $LOG
sync; sync

echo "$APT_GET install -y --force-yes sunwcs bash dpkg system-kernel dash"  | $TEE -a $LOG
$APT_GET install -y --force-yes sunwcs bash dpkg system-kernel dash 2>&1 | $TEE -a $LOG
sync; sync

echo "$APT_GET install -y --force-yes $APTINST" | $TEE -a $LOG
$APT_GET install -y --force-yes $APTINST 2>&1 | $TEE -a $LOG 

echo "$APT_GET clean" | $TEE -a $LOG
$APT_GET clean 2>&1 | $TEE -a $LOG 
sync; sync

for pid in `ps -ef|nawk '/devfsadmd/ {print $2}'`; do
	if pfiles $pid 2>/dev/null | grep $1 >/dev/null; then
		kill -9 $pid 2>/dev/null
		break
	fi
done

$SLEEP 2
umount $1/devices 2>/dev/null
#umount -f $1$2 2>/dev/null
#if test "x$3" = "x1"; then
#	umount -f $1/var/cache/apt/archives
#	rm -rf /tmp/apt-archive.$$
#fi

cp -f $LOG $1/root

# temporary fixes
#mkdir -p $1/usr/perl5/bin
#cd $1/usr/perl5/bin; ln -sf /usr/bin/perl perl

# update locale
echo "Setup en_US.UTF-8 locale ..." | $TEE -a $LOG
echo "LANG=en_US.UTF-8" >> "/etc/default/init"
echo "LC_ALL=en_US.UTF-8" >> "/etc/default/init"

echo "Removing stamp file: $STAMP ..." | $TEE -a $LOG
test -f $STAMP && rm -f $STAMP
sync; sync

exit 0
