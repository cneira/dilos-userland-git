INSTDEPENDS += debhelper
INSTDEPENDS += gnome-pkg-tools
#Build-Depends-Indep:
INSTDEPENDS += xsltproc
INSTDEPENDS += libxml2-utils
INSTDEPENDS += docbook-xml
INSTDEPENDS += docbook-xsl
INSTDEPENDS += highlight
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += pkg-config
# perl (>= 5.18),
INSTDEPENDS += python
INSTDEPENDS += yelp-tools
INSTDEPENDS += bc
INSTDEPENDS += dblatex
