INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += bison
INSTDEPENDS += autotools-dev
INSTDEPENDS += flex
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += xsltproc
INSTDEPENDS += libdbus-1-dev
# dbus
INSTDEPENDS += libgirepository1.0-dev
