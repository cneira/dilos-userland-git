INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxext-dev
INSTDEPENDS += libxi-dev
INSTDEPENDS += x11proto-record-dev
INSTDEPENDS += x11proto-xext-dev
INSTDEPENDS += x11proto-input-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += xmlto
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += xutils-dev
INSTDEPENDS += quilt
# specs:
INSTDEPENDS += xmlto
INSTDEPENDS += xorg-sgml-doctools
INSTDEPENDS += w3m
