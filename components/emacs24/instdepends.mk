INSTDEPENDS += debhelper
# bsd-mailx | mailx
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += texinfo
INSTDEPENDS += liblockfile-dev
INSTDEPENDS += librsvg2-dev
INSTDEPENDS += libgif-dev
# | libungif4-dev
INSTDEPENDS += libtiff-dev
INSTDEPENDS += xaw3dg-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libm17n-dev
INSTDEPENDS += libotf-dev
INSTDEPENDS += libdbus-1-dev
INSTDEPENDS += autoconf
INSTDEPENDS += automake
INSTDEPENDS += autotools-dev
INSTDEPENDS += dpkg-dev
INSTDEPENDS += quilt
INSTDEPENDS += libxaw7-dev
INSTDEPENDS += sharutils
INSTDEPENDS += imagemagick
INSTDEPENDS += libgtk-3-dev
INSTDEPENDS += libgnutls28-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libmagick++-dev
INSTDEPENDS += libgconf2-dev
# libasound2-dev [!hurd-i386 !kfreebsd-i386 !kfreebsd-amd64],
# libacl1-dev
INSTDEPENDS += zlib1g-dev
