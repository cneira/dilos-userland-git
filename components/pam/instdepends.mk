INSTDEPENDS += debhelper
INSTDEPENDS += libcrack2-dev
# bzip2
INSTDEPENDS += quilt
INSTDEPENDS += flex
INSTDEPENDS += libdb-dev
# libselinux1-dev [linux-any]
INSTDEPENDS += po-debconf
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += autopoint
# libaudit-dev [linux-any]
INSTDEPENDS += pkg-config
INSTDEPENDS += libfl-dev
INSTDEPENDS += docbook-xsl
INSTDEPENDS += docbook-xml
INSTDEPENDS += xsltproc
INSTDEPENDS += libxml2-utils
INSTDEPENDS += w3m
#Build-Conflicts-Indep:
INSTDEPENDS += fop
#
INSTDEPENDS += libcrypt-dev
