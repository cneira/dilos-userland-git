INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
# pypy
# pypy-setuptools
# pypy-six
INSTDEPENDS += python-all
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-six
INSTDEPENDS += python3-all
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-six
