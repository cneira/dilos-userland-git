INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += dh-python
INSTDEPENDS += gnome-pkg-tools
# netbase
INSTDEPENDS += cmake
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += python-all-dev
INSTDEPENDS += python3-all
INSTDEPENDS += libdbus-1-dev
#INSTDEPENDS += libkf5config-bin
#INSTDEPENDS += libmozjs185-dev
#INSTDEPENDS += libwebkit2gtk-4.0-dev
#INSTDEPENDS += libjavascriptcoregtk-4.0-dev
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libxmu-dev
