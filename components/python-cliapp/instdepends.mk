INSTDEPENDS += debhelper
INSTDEPENDS += python-all
INSTDEPENDS += python-coverage-test-runner
# python-sphinx
INSTDEPENDS += pep8
INSTDEPENDS += pylint
INSTDEPENDS += python-yaml
INSTDEPENDS += python-meliae
INSTDEPENDS += python-xdg
