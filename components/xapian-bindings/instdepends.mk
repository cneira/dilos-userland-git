INSTDEPENDS += debhelper
INSTDEPENDS += javahelper
# default-jdk
INSTDEPENDS += fastjar
INSTDEPENDS += ruby-all-dev
INSTDEPENDS += ruby
INSTDEPENDS += ruby-test-unit
INSTDEPENDS += dh-python
INSTDEPENDS += python
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-sphinx
INSTDEPENDS += python3
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3-sphinx
INSTDEPENDS += tcl-dev
INSTDEPENDS += libxapian-dev
INSTDEPENDS += autotools-dev
