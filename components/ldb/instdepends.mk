INSTDEPENDS += debhelper
INSTDEPENDS += docbook-xml
INSTDEPENDS += docbook-xsl
INSTDEPENDS += libldap2-dev
INSTDEPENDS += libpopt-dev
INSTDEPENDS += libtalloc-dev
INSTDEPENDS += libtdb-dev
INSTDEPENDS += libtevent-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += python
# python-all-dbg
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-talloc-dev
INSTDEPENDS += python-tdb
INSTDEPENDS += xsltproc
