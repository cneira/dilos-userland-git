INSTDEPENDS += debhelper
INSTDEPENDS += bison
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += flex
INSTDEPENDS += libdb-dev
INSTDEPENDS += libedit-dev
INSTDEPENDS += libhesiod-dev
INSTDEPENDS += libjson-perl
INSTDEPENDS += libldap2-dev
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += libperl4-corelibs-perl
INSTDEPENDS += libsqlite3-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxau-dev
INSTDEPENDS += libxt-dev
# netbase
INSTDEPENDS += pkg-config
INSTDEPENDS += python
INSTDEPENDS += comerr-dev
INSTDEPENDS += ss-dev
INSTDEPENDS += texinfo
# unzip
INSTDEPENDS += x11proto-core-dev
