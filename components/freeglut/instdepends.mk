INSTDEPENDS += debhelper
INSTDEPENDS += autoconf
INSTDEPENDS += automake
INSTDEPENDS += autotools-dev
INSTDEPENDS += dh-autoreconf
# libgl1-mesa-dev | libgl-dev,
INSTDEPENDS += libgl1-mesa-dev
# libglu1-mesa-dev | libglu-dev,
INSTDEPENDS += libglu1-mesa-dev
INSTDEPENDS += libtool
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxext-dev
INSTDEPENDS += libxi-dev
INSTDEPENDS += libxt-dev
INSTDEPENDS.i386 += libxxf86vm-dev
