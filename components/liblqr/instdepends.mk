INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += xsltproc
INSTDEPENDS += docbook-xsl
INSTDEPENDS += docbook-xml
INSTDEPENDS += pkg-config
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += dh-autoreconf
