INSTDEPENDS += debhelper
INSTDEPENDS += ant
INSTDEPENDS += ant-optional
# default-jdk
# default-jdk-doc
INSTDEPENDS += javahelper
INSTDEPENDS += libffi-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxt-dev
INSTDEPENDS += maven-repo-helper
INSTDEPENDS += pkg-config
