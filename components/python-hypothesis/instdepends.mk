INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
# pypy
# pypy-setuptools
INSTDEPENDS += python-all
INSTDEPENDS += python-enum34
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-all
INSTDEPENDS += python3-setuptools
# python3-sphinx
# python3-sphinx-rtd-theme
