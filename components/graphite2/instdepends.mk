INSTDEPENDS += debhelper
INSTDEPENDS += dh-exec
INSTDEPENDS += dpkg-dev
INSTDEPENDS += cmake
INSTDEPENDS += python
INSTDEPENDS += fonttools
#INSTDEPENDS += libmodule-build-perl
#Build-Depends-Indep:
INSTDEPENDS += asciidoc
# dblatex
INSTDEPENDS += doxygen
INSTDEPENDS += docbook-xsl
# latex-xcolor
INSTDEPENDS += libxml2-utils
# graphviz
