INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
INSTDEPENDS += python3-all
# pypy
INSTDEPENDS += python-pretend
INSTDEPENDS += python3-pretend
# pypy-pretend
INSTDEPENDS += python-pyparsing
INSTDEPENDS += python3-pyparsing
# pypy-pyparsing
INSTDEPENDS += python-six
INSTDEPENDS += python3-six
# pypy-six
INSTDEPENDS += python-pytest
INSTDEPENDS += python3-pytest
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-setuptools
# pypy-setuptools
