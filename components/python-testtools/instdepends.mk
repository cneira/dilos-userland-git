INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += openstack-pkg-tools
INSTDEPENDS += python-all
INSTDEPENDS += python-pbr
INSTDEPENDS += python-setuptools
# python-sphinx
INSTDEPENDS += python3-all
INSTDEPENDS += python3-pbr
INSTDEPENDS += python3-setuptools
#Build-Depends-Indep:
#INSTDEPENDS += python-extras
#INSTDEPENDS += python-linecache2
#INSTDEPENDS += python-mimeparse
#INSTDEPENDS += python-testresources
#INSTDEPENDS += python-traceback2
#INSTDEPENDS += python-twisted
#INSTDEPENDS += python-unittest2
#INSTDEPENDS += python3-extras
#INSTDEPENDS += python3-linecache2
#INSTDEPENDS += python3-mimeparse
#INSTDEPENDS += python3-testresources
#INSTDEPENDS += python3-traceback2
#INSTDEPENDS += python3-unittest2
