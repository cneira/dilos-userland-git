INSTDEPENDS += libncurses5-dev
INSTDEPENDS += autoconf
INSTDEPENDS += autotools-dev
INSTDEPENDS += bison
INSTDEPENDS += libncurses5-dev
INSTDEPENDS += texinfo
INSTDEPENDS += texi2html
# locales
INSTDEPENDS += gettext
INSTDEPENDS += sharutils
# time
INSTDEPENDS += xz-utils
INSTDEPENDS += dpkg-dev
#Build-Depends-Indep: texlive-latex-base, ghostscript, texlive-fonts-recommended
