INSTDEPENDS += debhelper
#Build-Depends-Indep:
INSTDEPENDS += python-all
INSTDEPENDS += python3-all
# python-doc
INSTDEPENDS += dh-python
# python-sphinx
# needed for build-time tests
#INSTDEPENDS += python-cherrypy
#INSTDEPENDS += python-eventlet
#INSTDEPENDS += python-flup
#INSTDEPENDS += python-gevent
#INSTDEPENDS += python-jinja2
#INSTDEPENDS += python-mako
#INSTDEPENDS += python-paste
#INSTDEPENDS += python-simpletal
#INSTDEPENDS += python-tornado
#INSTDEPENDS += python-tox
#INSTDEPENDS += python-twisted
#INSTDEPENDS += python-werkzeug
#INSTDEPENDS += gunicorn
