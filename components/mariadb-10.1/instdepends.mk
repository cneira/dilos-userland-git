INSTDEPENDS += debhelper
INSTDEPENDS += bison
INSTDEPENDS += chrpath
INSTDEPENDS += cmake
# dh-apparmor
INSTDEPENDS += dh-exec
# dh-systemd,
# gdb,
# libaio-dev [linux-any],
INSTDEPENDS += libboost-dev
INSTDEPENDS += libcrack2-dev
# libjemalloc-dev [linux-any],
INSTDEPENDS += libjudy-dev
INSTDEPENDS += libkrb5-dev
INSTDEPENDS += libncurses5-dev
# libpam0g-dev
INSTDEPENDS += libpam-dev
INSTDEPENDS += libpcre3-dev
INSTDEPENDS += libreadline-gplv2-dev
# libsystemd-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += lsb-release
# perl
INSTDEPENDS += po-debconf
# psmisc
INSTDEPENDS += unixodbc-dev
INSTDEPENDS += zlib1g-dev

INSTDEPENDS += libmtmalloc-dev
