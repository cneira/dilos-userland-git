INSTDEPENDS += debhelper
INSTDEPENDS += expat
#Build-Depends-Indep
# perl
INSTDEPENDS += libxml-parser-perl
INSTDEPENDS += libunicode-map8-perl
INSTDEPENDS += libunicode-string-perl
INSTDEPENDS += libtie-ixhash-perl
INSTDEPENDS += libxml-xpathengine-perl
# | libxml-xpath-perl
INSTDEPENDS += libtest-pod-perl
INSTDEPENDS += libtest-pod-coverage-perl
INSTDEPENDS += libxml-handler-yawriter-perl
INSTDEPENDS += libxml-sax-machines-perl
INSTDEPENDS += libxml-simple-perl
INSTDEPENDS += libyaml-perl
