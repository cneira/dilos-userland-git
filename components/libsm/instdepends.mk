INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += pkg-config
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += xtrans-dev
INSTDEPENDS += libice-dev
# uuid-dev
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += xutils-dev
INSTDEPENDS += quilt
# doc:
INSTDEPENDS += xmlto
INSTDEPENDS += xorg-sgml-doctools
#w3m
INSTDEPENDS += libuuid-dev
