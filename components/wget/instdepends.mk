INSTDEPENDS += debhelper
INSTDEPENDS += gettext
INSTDEPENDS += texinfo
INSTDEPENDS += autotools-dev
INSTDEPENDS += libidn11-dev
# uuid-dev
INSTDEPENDS += libpsl-dev
INSTDEPENDS += libpcre3-dev
INSTDEPENDS += libgnutls28-dev
INSTDEPENDS += automake
INSTDEPENDS += libssl-dev

INSTDEPENDS += libuuid-dev
