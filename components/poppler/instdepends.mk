INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libfontconfig1-dev
# libqt4-dev
INSTDEPENDS += libcairo2-dev
INSTDEPENDS += libopenjp2-7-dev
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libtiff-dev
INSTDEPENDS += liblcms2-dev
INSTDEPENDS += libfreetype6-dev
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += pkg-config
INSTDEPENDS += libgirepository1.0-dev
INSTDEPENDS += gobject-introspection
# qtbase5-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libnss3-dev
# libglib2.0-doc
# libcairo2-doc
