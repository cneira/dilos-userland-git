INSTDEPENDS += debhelper
INSTDEPENDS += dh-exec
INSTDEPENDS += autoconf
INSTDEPENDS += automake
INSTDEPENDS += bison
INSTDEPENDS += cluster-glue-dev
INSTDEPENDS += dh-python
INSTDEPENDS += docbook-xml
INSTDEPENDS += docbook-xsl
INSTDEPENDS += flex
# iproute
# inetutils-ping | iputils-ping | ping,
INSTDEPENDS += libbz2-dev
INSTDEPENDS += libcurl4-openssl-dev
# | libcurl3-openssl-dev,
INSTDEPENDS += libglib2.0-dev
INSTDEPENDS += libgnutls28-dev
INSTDEPENDS += libltdl3-dev
INSTDEPENDS += libncurses5-dev
#INSTDEPENDS += libopenhpi-dev
#INSTDEPENDS += libopenipmi-dev
# libpam0g-dev
INSTDEPENDS += libpam-dev
INSTDEPENDS += libpils2-dev
INSTDEPENDS += libplumb2-dev
INSTDEPENDS += libplumbgpl2-dev
INSTDEPENDS += libsensors4-dev
# | libsensors-dev,
INSTDEPENDS += libstonith1-dev
INSTDEPENDS += libtool
INSTDEPENDS += libxml2-dev
INSTDEPENDS += lynx
# net-tools
# openssh-client
# perl
# psmisc
INSTDEPENDS += python-dev
INSTDEPENDS += resource-agents
INSTDEPENDS += swig
INSTDEPENDS += libuuid-dev
INSTDEPENDS += xsltproc
INSTDEPENDS += zlib1g-dev
