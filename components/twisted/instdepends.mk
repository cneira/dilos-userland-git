INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
# patch
INSTDEPENDS += python-all-dev
# python-zope.interface-dbg (>= 4.0.2),
INSTDEPENDS += python-zope.interface
INSTDEPENDS += python-setuptools
# python-doc
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3-zope.interface
INSTDEPENDS += python3-setuptools
# python3-doc
#Build-Depends-Indep: python3-sphinx
