INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-zope.exceptions
INSTDEPENDS += python-zope.interface
INSTDEPENDS += python3-all
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-zope.exceptions
INSTDEPENDS += python3-zope.interface
