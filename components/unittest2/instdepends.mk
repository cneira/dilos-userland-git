INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += python-all
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-all
INSTDEPENDS += python3-setuptools
#Build-Depends-Indep:
INSTDEPENDS += python-linecache2
INSTDEPENDS += python-pkg-resources
INSTDEPENDS += python-six
INSTDEPENDS += python-traceback2
INSTDEPENDS += python3-linecache2
INSTDEPENDS += python3-pkg-resources
INSTDEPENDS += python3-six
INSTDEPENDS += python3-traceback2
