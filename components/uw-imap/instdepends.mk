INSTDEPENDS += debhelper
INSTDEPENDS += cdbs
INSTDEPENDS += dh-buildinfo
INSTDEPENDS += licensecheck
INSTDEPENDS += libpam-dev
INSTDEPENDS += krb5-multidev
INSTDEPENDS += comerr-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += d-shlibs
#
INSTDEPENDS += dh-exec
