INSTDEPENDS += debhelper
INSTDEPENDS += lsb-release
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libaprutil1-dev
INSTDEPENDS += libapr1-dev
INSTDEPENDS += libpcre3-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libnghttp2-dev
#INSTDEPENDS += libssl1.0-dev
INSTDEPENDS += libssl-dev
# perl
INSTDEPENDS += liblua5.2-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += autotools-dev
# gawk | awk
INSTDEPENDS += dtrace
INSTDEPENDS += dh-exec
