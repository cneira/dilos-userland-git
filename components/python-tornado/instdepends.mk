INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
INSTDEPENDS += python-all-dev
INSTDEPENDS += python-backports-abc
INSTDEPENDS += python-concurrent.futures
INSTDEPENDS += python-mock
INSTDEPENDS += python-pycurl
INSTDEPENDS += python-setuptools
INSTDEPENDS += python-singledispatch
INSTDEPENDS += python-twisted
INSTDEPENDS += python3-all-dev
INSTDEPENDS += python3
# python3-backports-abc
# python3-doc
INSTDEPENDS += python3-mock
INSTDEPENDS += python3-pycurl
INSTDEPENDS += python3-setuptools
INSTDEPENDS += python3-singledispatch
# python3-sphinx
# python3-sphinx-rtd-theme
INSTDEPENDS += python3-twisted
