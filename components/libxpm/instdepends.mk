INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += quilt
INSTDEPENDS += dpkg-dev
INSTDEPENDS += automake
INSTDEPENDS += libtool
INSTDEPENDS += xutils-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxt-dev
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += libxext-dev
INSTDEPENDS += pkg-config
