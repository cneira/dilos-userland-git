INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += automake
INSTDEPENDS += autoconf
INSTDEPENDS += libtool
INSTDEPENDS += libltdl-dev
INSTDEPENDS += autotools-dev
# gfortran
INSTDEPENDS += gcc-6
INSTDEPENDS += chrpath
INSTDEPENDS += libhwloc-dev
INSTDEPENDS += pkg-config
# libibverbs-dev (>= 1.1.7) [!kfreebsd-i386 !kfreebsd-amd64 !hurd-i386],
# libfabric-dev [amd64 i386],
# libcr-dev [amd64 armel armhf i386 powerpc], 
# libnuma-dev [amd64 i386 ia64 mips mipsel mipsn32 mipsn32el mips64 mips64el powerpc ppc64el],
# default-jdk
INSTDEPENDS += libsendfile-dev
