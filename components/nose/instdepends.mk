INSTDEPENDS += debhelper
INSTDEPENDS += dh-python
#INSTDEPENDS += libjs-jquery-hotkeys
#INSTDEPENDS += libjs-jquery-isonscreen
#INSTDEPENDS += libjs-jquery-tablesorter
INSTDEPENDS += python-all
INSTDEPENDS += python-setuptools
# python-sphinx
INSTDEPENDS += python-coverage
INSTDEPENDS += python3-all
INSTDEPENDS += python3-setuptools
