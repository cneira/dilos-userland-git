INSTDEPENDS += debhelper
INSTDEPENDS += python-all-dev
INSTDEPENDS += python3-all-dev
# python-pyrex
INSTDEPENDS += libxml2-dev
INSTDEPENDS += libxslt1-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += python-setuptools
INSTDEPENDS += python3-setuptools
# python-bs4
# python3-bs4
# python-html5lib
# python3-html5lib
# cython
# cython3
