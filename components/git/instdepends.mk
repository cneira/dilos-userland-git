INSTDEPENDS += debhelper
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libpcre3-dev
INSTDEPENDS += gettext
INSTDEPENDS += libcurl4-gnutls-dev
INSTDEPENDS += libexpat1-dev
INSTDEPENDS += subversion
INSTDEPENDS += libsvn-perl
INSTDEPENDS += libyaml-perl
INSTDEPENDS += tcl
INSTDEPENDS += libhttp-date-perl 
#| libtime-modules-perl
INSTDEPENDS += python
INSTDEPENDS += cvs
INSTDEPENDS += cvsps
INSTDEPENDS += libdbd-sqlite3-perl
INSTDEPENDS += unzip
INSTDEPENDS += libio-pty-perl
INSTDEPENDS += dh-exec
INSTDEPENDS += dh-apache2
INSTDEPENDS += dpkg-dev
# doc
INSTDEPENDS += asciidoc
INSTDEPENDS += xmlto
INSTDEPENDS += docbook-xsl
#
INSTDEPENDS += libintl-dev
